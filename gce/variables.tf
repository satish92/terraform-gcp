variable vm-name {
    type = string
    default  = instance-00
}

variable region{
    type = string
    default = us-central1
}