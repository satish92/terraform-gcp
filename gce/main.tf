resource "google_compute_instance" "default" {
name = var.vm-name
region
zone
}

boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
}

network_interface {
network = "default"
}

resource "google_compute_firewall" "http-server" {
    name = "default-allow-http-server"
    network = "default"

 allow {
     protocol = "tcp"
     ports = ["80"]
 }
 source_ranges= ["0.0.0.0/0"]
 target_tags = ["http-server"]
}

output.ip {
    value = "${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}"
}